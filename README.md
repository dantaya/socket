# Socket Server

Build a socket server base that can be adapted to fit any number of use cases.

### How?

This takes a few parameters that identify the user and the channel the user is in.

Once in a channel, any messages sent to the server by somone in the channel get broadcast to all other users in the same channel.

### Use cases

- Live chat application with channels
- Live editing of files with multiple people 
