package channel

import (
	"net"
)

// User holds the useres data
type User struct {
	Start      int64
	Last       int64
	Connection net.Conn
}

// channel defines what a channel is
type channel struct {
	Map map[string]map[string]User
}

// Channel map
var Channel *channel

// Init create the channel object
func Init() error {
	Channel = new(channel)
	Channel.Map = make(map[string]map[string]User)
	return nil
}

// AddUser to a channel
func AddUser(key string, id string, conn net.Conn) error {
	// Check to make sure the key exists in the map if it doesnt create it
	if _, ok := Channel.Map[key]; !ok {
		Channel.Map[key] = make(map[string]User)
	}

	// Check to see if the user exists if not create them
	if _, ok := Channel.Map[key][id]; !ok {
		user := User{}
		user.Connection = conn

		Channel.Map[key][id] = user
	}

	return nil
}

// RemoveUser from a channel
func RemoveUser() error {
	return nil
}

// Broadcast a message to a channel
func Broadcast() error {
	return nil
}

// open a new channel
func (ch *channel) open(key string) error {
	// Check to make sure the key exists in the map if it doesnt create it
	if _, ok := Channel.Map[key]; !ok {
		Channel.Map[key] = make(map[string]User)
	}

	return nil
}

// close a channel
func (ch *channel) close(key string) error {

	// Delete the channel from the map
	delete(ch.Map, key)

	return nil
}
